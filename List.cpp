/*
Gizatullin Airat. Group: 09-404(2)
Not Finished!
*/

#include <iostream>
using namespace std;

typedef int T;

struct Node
{
	T date;
	Node* next;
	Node* prev;
	
	Node(T info = 0){
		date = info;
		next = NULL;
		prev = NULL;
	}
};

struct Ring
{
private:
	Node head;
	int size;
public:
	Ring(){
		size = 0;
	}

	void push_front(T arg){
		Node* temp = new Node(arg);
		if (size == 0){
			temp->next = &head;
			temp->prev = &head;
			head.next = temp;
			head.prev = temp;
		}
		else{
			temp->next = head.next;
			temp->prev = &head;
			head.next->prev = temp;
			head.next = temp;
		}
		size++;
	}

	void push_back(T arg){
		if (size == 0){
			push_front(arg);
		}
		else{
			Node* temp = new Node(arg);
			temp->prev = head.prev;
			temp->next = &head;
			head.prev->next = temp;
			head.prev = temp;
			size++;
		}
	}

	void push_mid(T arg, int id){
		if (size == 0){
			push_back(arg);
		}
		else{
			if (id == 0)
				push_front(arg);
			else
				if (id >= size)
					push_back(arg);
				else{
					Node *current = head.next;
					Node *temp = new Node(arg);
					for (int i = 0; i < id - 1; i++){
						current = current->next;
					}
					current->next->prev = temp;
					temp->next = current->next;
					temp->prev = current;
					current->next = temp;
					size++;
				}
		}
	}

	void print(){
		cout << "-------------------------------" << endl;
		if (size == 0){
			cout << "Ring is empty";
		}
		else{
			Node *current = head.next;
			for (int i = 0; i < size; i++){
				cout << current->date << " ";
				current = current->next;
			}
		}
		cout << endl << "-------------------------------" << endl;
	}

	int count(){
		return size;
	}

	bool empty(){
		return (size == 0);
	}

	void search_elem(int adress){
		if (size == 0)
			cout << "Ring is empty";
		else{
			Node *current = head.next;
			for (int i=0; i < adress-1; i++)
				current = current->next;
			cout << current->date<< endl;
			cout << "------------------------------" << endl;
		}
	}
	
	void delete_elem(){
		Ring ring;
		int id;
		cout << "Enter adress for delete elem = ";
		cin >> id;
		if (size == 0)
			cout << "Ring is empty";
		else{
			if (id > size || id <0)
				cout << "Error!!!"<< endl;
			else{
				cout << "This is elem delete = ";
				Node *current = head.next;
				for (int i = 0; i < id - 1; i++)
					current = current->next;
				cout << current->date << endl;
				cout << "------------------------------" << endl;
				current->next->prev = current->prev;
				current->prev->next = current->next;
				delete current;
				size--;
			}
		}
	}

	
	
	void Input(int enter_size){
		int Ask, id;
		T arg;
		for (int i = 0; i < enter_size; i++){
			cout << "Push? \n1)Begin \n2)End \n3)Middle \nPlease, enter number = ";
			cin >> Ask;
			switch (Ask)
			{
			case 1: 
				cout << "Enter argument = ";
				cin >> arg;
				push_front(arg);	break;
			case 2:
				cout << "Enter argument = ";
				cin >> arg;
				push_back(arg);		break;
			case 3:
				cout << "Enter push adress = ";
				cin >> id;
				cout << "Enter argument = ";
				cin >> arg;
				push_mid(arg, id);		break;
			default:
				cout << "Enter argument = ";
				cin >> arg;
				push_back(arg);		break;
			}
		}
	}


};

int main()
{
	int size;
	cout << "Enter size = ";
	cin >> size;
	Ring ring;
	/*
	ring.push_back(4);
	ring.push_back(67);
	ring.push_back(234);
	ring.push_back(1);
	ring.push_front(2);
	ring.push_front(22);
	ring.print();
	ring.delete_elem();
	ring.print();
	ring.push_mid(12);
	ring.print();
	ring.search_elem(adress);
	*/
	ring.Input(size);
	ring.print();
	ring.delete_elem();
	ring.print();
	system("pause");
	return 0;
}